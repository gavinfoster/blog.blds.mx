import React from 'react'
import { graphql } from 'gatsby'
import 'wysiwyg.css'
import { documentToHtmlString } from '@contentful/rich-text-html-renderer';
import get from 'lodash/get'
import Helmet from 'react-helmet'
import '../reset.css'
import '../styles/styles.scss'

const Home = (props) => {
  const siteTitle = get(props, 'data.site.siteMetadata.title')
  const [ homePageEdge ] = get(props, 'data.allContentfulPage.edges')

  const {
    node: {
      title,
      content: {
        json: contentJson
      }
    }
  } = homePageEdge

  return (
    <div>
      <Helmet title={siteTitle} />
      <h1>here we go baby</h1>
      <div dangerouslySetInnerHTML={{__html: documentToHtmlString(contentJson)}}></div>
    </div>
  )
}

export default Home

export const pageQuery = graphql`
  query BlogIndexQuery {
    site {
      siteMetadata {
        title
      }
    }
    allContentfulPage {
      edges {
        node {
          title
          content {
            json
          }
        }
      }
    }
  }
`
